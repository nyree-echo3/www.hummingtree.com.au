<?php 
   // Set Meta Tags
   $meta_title_inner = "Make a Donation" . $company_name; 
   $meta_keywords_inner = "donation, " . $company_name; 
   $meta_description_inner = "Make a Donation | " . $company_name;  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">      
        @include('site/partials/sidebar-donation')
                     
        <div class="col-sm-8 blog-main">

          <div class="blog-post">   
               <h1 class="blog-post-title">Donation</h1>    
               @include('flash::message')           
            </div>
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
@endsection            
