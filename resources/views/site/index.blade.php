@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel')
@include('site/partials/index-panel-signup')
@include('site/partials/index-panel-online')
@include('site/partials/index-panel-seo')
@include('site/partials/index-panel-community')
@include('site/partials/index-panel-newsletter')
@endsection
