<footer class='footer'>     
  <div class='footerContainer'>
      <div id="footer-menu">                 
         <div class="container-fluid">
		    <div class="row justify-content-center align-items-center">         			 
			    <div class="col-lg-2">			  
				   <a href="{{ url('') }}/index" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo-hummingtree3bottom-black.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
			    </div>
			  
			    <div class="col-xl-4 col-lg-6 col-sm-12">
			  	   <ul>
					  <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="{{ url('') }}">Home</a></li>
					  {!! $navigation !!}
				   </ul>
			    </div>
		     </div>			 
	      </div>
	      
	      <div id="footer-social">  
			 @if ( $social_facebook != "") <div><a href="{{ $social_facebook }}" target="_blank"><i class='fab fa-facebook-f'></i></a></div> @endif
			 @if ( $social_twitter != "") <div><a href="{{ $social_twitter }}" target="_blank"><i class='fab fa-twitter'></i></a></div> @endif 
			 @if ( $social_linkedin != "") <div><a href="{{ $social_linkedin }}" target="_blank"><i class='fab fa-linkedin-in'></i></a></div> @endif
			 @if ( $social_googleplus != "") <div><a href="{{ $social_googleplus }}" target="_blank"><i class='fab fa-google-plus-g'></i></a></div> @endif
			 @if ( $social_instagram != "") <div><a href="{{ $social_instagram }}" target="_blank"><i class='fab fa-instagram'></i></a></div> @endif
			 @if ( $social_pinterest != "") <div><a href="{{ $social_pinterest }}" target="_blank"><i class='fab fa-pinterest-p'></i></a></div> @endif
			 @if ( $social_youtube != "") <div><a href="{{ $social_youtube }}" target="_blank"><i class='fab fa-youtube'></i></a></div> @endif	
		 </div> 
      </div>	   
 
	  <div id="footer-txt"> 
		  <div><a href="{{ url('') }}">&copy; {{ date('Y') }} {{ $company_name }}</a><span> |</span></div>
		  <div><a href="{{ url('') }}/contact">Contact</a><span> |</span></div>  
		  <div><a href="https://www.echo3.com.au" target="_blank">Website by Echo3</a></div>     
	  </div>
	  
  </div>
</footer>