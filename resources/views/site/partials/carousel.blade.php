@php
   $randomImage = rand(0, sizeof($images)-1);     
@endphp

<div id="sliderParallax" class="sliderParallaxParent sliderParallax-container">   
	<div class="sliderParallax-img sliderParallax-img-main">
		<div class="sliderParallax-img-img"></div>		
	</div>
	<div class="sliderParallax-overlay">	
	<div class="sliderParallax-txt">
        @if ( $images[$randomImage]->title != "") 
           <img src="{{ url('') }}/images/site/logo-hummingtree1a.png" title="{{ $company_name }}" alt="{{ $company_name }}">
		   <h2>{{ $images[$randomImage]->title }}</h2>
	    @endif	    	    
	    
	    @if ( $images[$randomImage]->description != "") 
		   <p>{{ $images[$randomImage]->description }}</p>
	    @endif
	    
	    <div class="slider-btn">
		   <a href="{{ url('') }}">See more about Hummingtree here.</a>
	    </div>	    				    			
	</div>
	
	</div>
</div>										
						
							
@section('scripts-slider-parallex')
<script type="text/javascript" src="{{ asset('/js/site/parallex/TweenMax.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/site/parallex/ScrollMagic.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/site/parallex/animation.gsap.js') }}"></script>
@endsection
										
@section('inline-scripts-slider-parallex') 
	<script>
		$( document ).ready(function() {
		   imageUrl = '{{ url('') }}/{{ $images[$randomImage]->location }}';
		   $('.sliderParallax-img').css('background-image', 'url(' + imageUrl + ')');
		   $('.sliderParallax-img-main').css('background-image', 'url(' + imageUrl + ')');
		
		
		// init controller
		var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

		// build scenes				
		new ScrollMagic.Scene({triggerElement: "#sliderParallax"})
						.setTween("#sliderParallax > div", {y: "80%", ease: Linear.easeNone})										
						.addTo(controller);
		});
	</script>
@endsection	