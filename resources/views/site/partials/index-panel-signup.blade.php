 <div class="index-panel-signup">
   <div class="container-fluid">
	  <div class="row">         			 
		  <div class="col-lg-4">
		      <div data-aos="fade-up" data-aos-duration="2000">			 
				  <div class="index-panel-signup-img">
					 <img src="{{ url('') }}/images/site/pic1.png" alt="Sign Up" >
				  </div>			

				  <div class="index-panel-signup-btn">
					 <a class="btn-submit" href="{{ url('') }}">Sign Up</a>
				  </div>
			  </div>
		  </div><!-- /.col-lg-4 -->		
		  
		  <div class="col-lg-4">
		      <div data-aos="fade-up" data-aos-duration="2000">				 
				  <div class="index-panel-signup-img">
					 <img src="{{ url('') }}/images/site/pic2.png" alt="Sign Up" >
				  </div>			

				  <div class="index-panel-signup-btn">
					 <a class="btn-submit" href="{{ url('') }}">Sign Up</a>
				  </div>
			  </div>
		  </div><!-- /.col-lg-4 -->		
		  
		  <div class="col-lg-4">
		      <div data-aos="fade-up" data-aos-duration="2000">				 
				  <div class="index-panel-signup-img">
					 <img src="{{ url('') }}/images/site/pic3.png" alt="Sign Up" >
				  </div>			

				  <div class="index-panel-signup-btn">
					 <a class="btn-submit" href="{{ url('') }}">Sign Up</a>
				  </div>
			  </div>
		  </div><!-- /.col-lg-4 -->		
		  	 
		</div><!-- /.row -->	
   </div><!-- /.container -->	
</div><!-- /.index-panel-signup -->	