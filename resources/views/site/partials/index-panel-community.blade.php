<div id="parallax" class="parallaxParent parallax-container">   
	<div class="parallax-img parallax-img-main">
		<div class="parallax-img-img"></div>		
	</div>
	<div class="parallax-overlay">
	<div class="parallax-txt">
		<h2>Be part of our community</h2>
		
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla.</p>
		
		<div class="index-panel-btn">
		   <a class="btn-submit" href="{{ url('') }}">Find Out More</a>
	    </div>
	</div>
	</div>
</div>										
						
							
@section('scripts-parallex')
<script type="text/javascript" src="{{ asset('/js/site/parallex/TweenMax.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/site/parallex/ScrollMagic.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/site/parallex/animation.gsap.js') }}"></script>
@endsection
										
@section('inline-scripts-parallex') 
	<script>
		// init controller
		var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

		// build scenes				
		new ScrollMagic.Scene({triggerElement: "#parallax"})
						.setTween("#parallax > div", {y: "80%", ease: Linear.easeNone})										
						.addTo(controller);
        //$(".parallax3-img").addClass('parallax3-img-mod');
		
	</script>
@endsection	