<div data-aos="fade-up" data-aos-duration="2000">	
	<div class="index-panel-online">
	   <div class="container-fluid h-100">
		  <div class="row no-gutters h-100 align-items-center background-50">         			 		 
			  <div class="index-panel-online-txt">
				  <h2>Online Yoga classes</h2>			 

				  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla.</p>

				  <div class="index-panel-btn">
					 <a class="btn-submit" href="{{ url('') }}">Sign Up</a>
				  </div>
			  </div>		

			</div><!-- /.row -->	
	   </div><!-- /.container -->	
	</div><!-- /.index-panel-online -->	
</div>