<div class="col-sm-3 offset-sm-1 blog-sidebar">
	<div class="sidebar-module">
		<h4>{{ $category[0]->name }}</h4>
		<ol class="list-group list-unstyled list-group-flush">
			{!! $side_nav !!}
		</ol>
	</div>
</div>