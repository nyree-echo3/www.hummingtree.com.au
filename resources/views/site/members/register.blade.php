<?php 
   // Set Meta Tags
   $meta_title_inner = "Register | " . $company_name; 
   $meta_keywords_inner = "Register " . $company_name; 
   $meta_description_inner = "Register " . $company_name; 
?>

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-members')        
        
        <div class="col-sm-8 blog-main">

          <div class="blog-post">            
            <h1 class="blog-post-title">Register</h1>                                    
            
            <p><a href='{{ url('')}}/login'>Already a member yet?  Please log in here.</a></p>
            
				<form id="frmRegister" method="POST" action="{{ url('') }}/register/store">
				    <input type="hidden" name="_token" value="{{ csrf_token() }}">
				    
				    <div class="form-group row">
						<label class="col-md-3 col-form-label">Membership *</label>
						<div class="col-md-9">							
							<select class="form-control" name="membershipType">
								<option value="">Choose your membership</option>
								@foreach ($member_types as $member_type)
								   <option value="{{ $member_type->id }}" {{ (old('membershipType') == $member_type->id ? " selected" : "") }}>{{ $member_type->name }} - ${{ $member_type->price }}</option>															
								@endforeach
							</select>
							
							@if ($errors->has('state'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('state') }}</div>
                                </div>
                            @endif
						</div>            
					</div>
				    
					<div class="form-group row">
						<label class="col-md-3 col-form-label">First Name *</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="firstName" placeholder="Your first name" value="{{ old('firstName') }}" />
							@if ($errors->has('firstName'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('firstName') }}</div>
                                </div>
                            @endif
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Last Name *</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="lastName" placeholder="Your last name" value="{{ old('lastName') }}" />
							@if ($errors->has('lastName'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('lastName') }}</div>
                                </div>
                            @endif
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Phone Number *</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="phoneNumber" placeholder="Your phone number (10 digits with no spaces)" value="{{ old('phoneNumber') }}" />
							@if ($errors->has('phoneNumber'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('phoneNumber') }}</div>
                                </div>
                            @endif
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Email *</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="email" placeholder="Your email" value="{{ old('email') }}" />
							@if ($errors->has('email'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('email') }}</div>
                                </div>
                            @endif
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Address *</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="address1" placeholder="Your address (line 1)" value="{{ old('address1') }}" />
							@if ($errors->has('address1'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('addresss1') }}</div>
                                </div>
                            @endif
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label"></label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="address2" placeholder="Your address (line 2)" value="{{ old('address2') }}" />
							@if ($errors->has('address2'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('address2') }}</div>
                                </div>
                            @endif
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Suburb *</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="suburb" placeholder="Your suburb" value="{{ old('suburb') }}" />
							@if ($errors->has('suburb'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('suburb') }}</div>
                                </div>
                            @endif
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">State *</label>
						<div class="col-md-4">							
							<select class="form-control" name="state">
								<option value="">Your state</option>
								<option value="ACT" {{ (old('state') == "ACT") ? ' selected="selected"' : '' }}>ACT</option>																
								<option value="NSW" {{ (old('state') == "NSW") ? ' selected="selected"' : '' }}>NSW</option>
								<option value="NT" {{ (old('state') == "NT") ? ' selected="selected"' : '' }}>NT</option>
								<option value="QLD" {{ (old('state') == "QLD") ? ' selected="selected"' : '' }}>QLD</option>
								<option value="SA" {{ (old('state') == "SA") ? ' selected="selected"' : '' }}>SA</option>
								<option value="TAS" {{ (old('state') == "TAS") ? ' selected="selected"' : '' }}>TAS</option>
								<option value="VIC" {{ (old('state') == "VIC") ? ' selected="selected"' : '' }}>VIC</option>
								<option value="WA" {{ (old('state') == "WA") ? ' selected="selected"' : '' }}>WA</option>
							</select>
							
							@if ($errors->has('state'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('state') }}</div>
                                </div>
                            @endif
						</div>            
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Postcode *</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="postcode" placeholder="Your postcode" value="{{ old('postcode') }}" />
							@if ($errors->has('postcode'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('postcode') }}</div>
                                </div>
                            @endif
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Password *</label>
						<div class="col-md-9">
							<input type="password" class="form-control" name="password" placeholder="Your password" />
							
							<div id="progressBarDiv">
								Strength
								<div class="progress mt-2" id="progressBar">
									<div class="progress-bar bg-secondary progress-bar-animate" style="width: 100%"></div>
								</div>
							</div>
							
							@if ($errors->has('password'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('password') }}</div>
                                </div>
                            @endif												
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Confirm Password *</label>
						<div class="col-md-9">
							<input type="password" class="form-control" name="passwordConfirm" placeholder="Confirm your password" />
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Confirmation *</label>
						<div class="col-md-9">
							<input type="checkbox" class="form-control" name="confirmation" />
							 <label class="form-control-label" for="confirmation">I agree and acknowledge the terms & conditions outlined in this agreement.  Your submission of this form confirms that you have read and agree with our terms & conditions.</label>
							 
							 @if ($errors->has('confirmation'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('confirmation') }}</div>
                                </div>
                            @endif
						</div>           
					</div>
					
					<div class="form-group row">
					    <label class="col-md-3 col-form-label"></label>
						<div class="col-md-4 g-recaptcha-container">
							<div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
							@if ($errors->has('g-recaptcha-response'))
							    <div class="fv-plugins-message-container">
								    <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('g-recaptcha-response') }}</div>
							    </div>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-9 offset-md-3">
							<button type="submit" class="btn-checkout" name="signup" value="Sign up">Submit</button>
						</div>
					</div>
				</form>
     
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->        
@endsection

                        
@section('scripts')                                                                        
<script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
<script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.4.2/zxcvbn.js"></script>
@endsection

@section('inline-scripts')

<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(e) { 			
		const strongPassword = function() {
			return {
				validate: function(input) {
					// input.value is the field value
					// input.options are the validator options

					const value = input.value;
					if (value === '') {
						return {
							valid: true,
						};
					}

					const result = zxcvbn(value);
					const score = result.score;
					const message = result.feedback.warning || 'The password is weak';

					// By default, the password is treat as invalid if the score is smaller than 3
					// We allow user to change this number via options.minimalScore
					const minimalScore = input.options && input.options.minimalScore
										? input.options.minimalScore
										: 3;

					if (score < minimalScore) {
						return {
							valid: false,
							// Yeah, this will be set as error message
							message: message,
							meta: {
								// This meta data will be used later
								score: score,
							},
						}
					}
				},
			};
		};
			        
		const form = document.getElementById('frmRegister');
		FormValidation.formValidation(form, {
			fields: {
				membershipType: {
					validators: {
						notEmpty: {
							message: 'The membership type is required'
						}
					}
				},  
				firstName: {
					validators: {
						notEmpty: {
							message: 'The first name is required'
						}
					}
				},        
				lastName: {
					validators: {
						notEmpty: {
							message: 'The last name is required'
						}
					}
				},        
				phoneNumber: {
					validators: {
						notEmpty: {
							message: 'The phone number is required'
						},
						digits: {
							message: 'The phone number is not valid.  It must only contain 10 digits with no spaces'
						},
						stringLength: {
							max:10,
							min:10,
							message: 'The phone number is not valid.  It must only contain 10 digits with no spaces'
						}
					}
				},        
				firstName: {
					validators: {
						notEmpty: {
							message: 'The first name is required'
						}
					}
				},        
				email: {
					validators: {
						notEmpty: {
							message: 'The email is required'
						},
						emailAddress: {
							message: 'The email is not valid'
						}
					}
				},        
				address1: {
					validators: {
						notEmpty: {
							message: 'The address is required'
						}
					}
				},        
				suburb: {
					validators: {
						notEmpty: {
							message: 'The suburb is required'
						}
					}
				},        
				state: {
					validators: {
						notEmpty: {
							message: 'The state is required'
						}
					}
				},        
				postcode: {
					validators: {
						notEmpty: {
							message: 'The postcode is required'
						},
						digits: {
							message: 'The postcode is not valid.  It must only contain 4 digits'
						},
						stringLength: {
							max:4,
							min:4,
							message: 'The phone number is not valid.  It must only contain 4 digits'
						}
					}
				},        
				password: {
					validators: {
						notEmpty: {
							message: 'The password is required'
						},
						checkPassword: {
                                message: 'The password is too weak',
                                minimalScore: 2,
                            }
					}
				},        
				passwordConfirm: {
					validators: {
						notEmpty: {
							message: 'The password confirmation is required'
						},
						identical: {
                            compare: function() {
                                return form.querySelector('[name="password"]').value;
                            },
                            message: 'The password and its confirm are not the same'
                        }
					}
				},       
				
				confirmation: {
					validators: {
						notEmpty: {
							message: 'The confirmation is required'
						}
					}
				},       
			},
			plugins: {
				trigger: new FormValidation.plugins.Trigger(),
				bootstrap: new FormValidation.plugins.Bootstrap(),
				submitButton: new FormValidation.plugins.SubmitButton(),
				defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
				icon: new FormValidation.plugins.Icon({
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh',
				}),
                },
            }
        )
        .registerValidator('checkPassword', strongPassword)
		
        .on('core.validator.validating', function(e) {
            if (e.field === 'password' && e.validator === 'checkPassword') {				
                document.getElementById('progressBarDiv').style.opacity = '1';				
            }
        })
        .on('core.validator.validated', function(e) {
            if (e.field === 'password' && e.validator === 'checkPassword') {
                const progressBar = document.getElementById('progressBar');

                if (e.result.meta) {
                    // Get the score which is a number between 0 and 4
                    const score = e.result.meta.score;
                    
                    // Update the width of progress bar
                    const width = (score == 0) ? '1%' : score * 25 + '%';
                    progressBar.style.opacity = 1;
                    progressBar.style.width = width;
                } else {
                    progressBar.style.opacity = 0;
                    progressBar.style.width = '0%';
                }
            }
        });		
});
</script>

@endsection