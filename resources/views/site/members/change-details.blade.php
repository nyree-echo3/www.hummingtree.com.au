<?php 
   // Set Meta Tags
   $meta_title_inner = "Change My Details | " . $company_name; 
   $meta_keywords_inner = "Change My Details " . $company_name; 
   $meta_description_inner = "Change My Details " . $company_name; 
?>

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-members-portal')        
        
        <div class="col-sm-8 blog-main">

          <div class="blog-post">            
            <h1 class="blog-post-title">Change My Details</h1>                                    
            @include('flash::message')   
                        
				<form id="frmRegister" method="POST" action="{{ url('') }}/members-portal/save-details">
				    <input type="hidden" name="_token" value="{{ csrf_token() }}">
				    				    
					<div class="form-group row">
						<label class="col-md-3 col-form-label">First Name *</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="firstName" placeholder="Your first name" value="{{ old('firstName', $member->firstName) }}" />
							@if ($errors->has('firstName'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('firstName') }}</div>
                                </div>
                            @endif
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Last Name *</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="lastName" placeholder="Your last name" value="{{ old('lastName',$member->lastName) }}" />
							@if ($errors->has('lastName'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('lastName') }}</div>
                                </div>
                            @endif
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Phone Number *</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="phoneNumber" placeholder="Your phone number (10 digits with no spaces)" value="{{ old('phoneMobile',$member->phoneMobile) }}" />
							@if ($errors->has('phoneNumber'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('phoneNumber') }}</div>
                                </div>
                            @endif
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Email *</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="email" placeholder="Your email" value="{{ old('email',$member->email) }}" />
							@if ($errors->has('email'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('email') }}</div>
                                </div>
                            @endif
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Address *</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="address1" placeholder="Your address (line 1)" value="{{ old('address1',$member->address1) }}" />
							@if ($errors->has('address1'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('addresss1') }}</div>
                                </div>
                            @endif
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label"></label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="address2" placeholder="Your address (line 2)" value="{{ old('address2',$member->address2) }}" />
							@if ($errors->has('address2'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('address2') }}</div>
                                </div>
                            @endif
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Suburb *</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="suburb" placeholder="Your suburb" value="{{ old('suburb',$member->suburb) }}" />
							@if ($errors->has('suburb'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('suburb') }}</div>
                                </div>
                            @endif
						</div>           
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">State *</label>
						<div class="col-md-4">							
							<select class="form-control" name="state">
								<option value="">Your state</option>
								<option value="ACT" {{ (old('state', $member->state) == "ACT" ? ' selected' : '') }}>ACT</option>																
								<option value="NSW" {{ (old('state', $member->state) == "NSW" ? ' selected' : '') }}>NSW</option>
								<option value="NT" {{ (old('state', $member->state) == "NT" ? ' selected' : '') }}>NT</option>
								<option value="QLD" {{ (old('state', $member->state) == "QLD" ? ' selected' : '') }}>QLD</option>
								<option value="SA" {{ (old('state', $member->state) == "SA" ? ' selected' : '') }}>SA</option>
								<option value="TAS" {{ (old('state', $member->state) == "TAS" ? ' selected' : '') }}>TAS</option>
								<option value="VIC" {{ (old('state', $member->state) == "VIC" ? ' selected' : '') }}>VIC</option>
								<option value="WA" {{ (old('state', $member->state) == "WA" ? ' selected' : '') }}>WA</option>
							</select>
							
							@if ($errors->has('state'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('state') }}</div>
                                </div>
                            @endif
						</div>            
					</div>
					
					<div class="form-group row">
						<label class="col-md-3 col-form-label">Postcode *</label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="postcode" placeholder="Your postcode" value="{{ old('postcode',$member->postcode) }}" />
							@if ($errors->has('postcode'))
                                <div class="fv-plugins-message-container">
                                   <div class="fv-plugins-bootstrap fv-help-block">{{ $errors->first('postcode') }}</div>
                                </div>
                            @endif
						</div>           
					</div>
															
					<div class="form-group row">
						<div class="col-md-9 offset-md-3">
							<button type="submit" class="btn-checkout" name="update" value="Update Details">Update</button>
						</div>
					</div>
				</form>
     
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->        
@endsection

                        
@section('scripts')                                                                        
<script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
<script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.4.2/zxcvbn.js"></script>
@endsection

@section('inline-scripts')

<script type="text/javascript">
	document.addEventListener('DOMContentLoaded', function(e) { 					
			        
		const form = document.getElementById('frmRegister');
		FormValidation.formValidation(form, {
			fields: {				
				firstName: {
					validators: {
						notEmpty: {
							message: 'The first name is required'
						}
					}
				},        
				lastName: {
					validators: {
						notEmpty: {
							message: 'The last name is required'
						}
					}
				},        
				phoneNumber: {
					validators: {
						notEmpty: {
							message: 'The phone number is required'
						},
						digits: {
							message: 'The phone number is not valid.  It must only contain 10 digits with no spaces'
						},
						stringLength: {
							max:10,
							min:10,
							message: 'The phone number is not valid.  It must only contain 10 digits with no spaces'
						}
					}
				},        
				firstName: {
					validators: {
						notEmpty: {
							message: 'The first name is required'
						}
					}
				},        
				email: {
					validators: {
						notEmpty: {
							message: 'The email is required'
						},
						emailAddress: {
							message: 'The email is not valid'
						}
					}
				},        
				address1: {
					validators: {
						notEmpty: {
							message: 'The address is required'
						}
					}
				},        
				suburb: {
					validators: {
						notEmpty: {
							message: 'The suburb is required'
						}
					}
				},        
				state: {
					validators: {
						notEmpty: {
							message: 'The state is required'
						}
					}
				},        
				postcode: {
					validators: {
						notEmpty: {
							message: 'The postcode is required'
						},
						digits: {
							message: 'The postcode is not valid.  It must only contain 4 digits'
						},
						stringLength: {
							max:4,
							min:4,
							message: 'The phone number is not valid.  It must only contain 4 digits'
						}
					}
				},        				
			},
			plugins: {
				trigger: new FormValidation.plugins.Trigger(),
				bootstrap: new FormValidation.plugins.Bootstrap(),
				submitButton: new FormValidation.plugins.SubmitButton(),
				defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
				icon: new FormValidation.plugins.Icon({
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh',
				}),
                },
            }
        )	
});
</script>

@endsection