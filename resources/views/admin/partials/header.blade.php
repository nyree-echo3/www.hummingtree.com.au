<header class="main-header">
    <a href="{{ url('dreamcms/dashboard') }}" class="logo">
        <span class="logo-mini"><b>CMS</b></span>
        <span class="logo-lg"><img src="{{ asset('images/admin/logo.png') }}" alt="Echo3" title="Echo3" class='navbar-logo'> Dream<b>CMS</b></span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"><i class="fa fa-bars"></i>
            <span class="sr-only">Toggle navigation</span>            
        </a>   
        <div class="navbar-company-name">
		   {{ $company_name }}
		</div>
       
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="{{ url('dreamcms/profile') }}">
                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                    </a>
                </li>
                <li>
                    <a href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <i class="fa fa-power-off"></i>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </div>
    </nav>
</header>
