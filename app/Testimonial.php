<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Rutorika\Sortable\SortableTrait;

class Testimonial extends Model
{
    use SortableTrait, Sortable;

    protected $table = 'testimonials';

    public $sortable = ['title', 'category_id', 'status'];

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }
   
    public function scopeFilter($query)
    {

        $filter = session()->get('testimonials-filter');
        $select = "";
       
        if($filter['search']){
            $select =  $query->where('title','like', '%'.$filter['search'].'%');
        }

        return $select;
    }
}
